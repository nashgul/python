# Clases 'class' en python

## ¿Qué son las clases?
En mi breve paso por python he descubierto, al fin, para qué y cómo trabajar con clases.
Tal y como yo lo veo, una clase define o construye un objeto en python.
Mi experiencia me dice que un objeto es algo parecido a una variable potente, potente en el sentido que no sólo guarda valores, llamados 'registros', sino que puede realizar acciones, a esas acciones se las llama 'métodos'.

### Voy a poner un ejemplo (a ver si lo hago mejor de lo que por ahí pone el personal):
- Creo la clase coche.
- Al crear la clase coche le puedo añadir unas propiedades:
Color = azul,
Puertas = 5,
Marca = Fiat.
- Pero también puedo hacer que el coche haga cosas o efectuar acciones sobre él:
Llenar el depósito,
Encerderlo,
Acelerar,
Pararlo.
....

### Ejemplo en la programación.
Pensemos en una ventana de tu entorno de escritorio.

- Crear una 'clase' ventana.
- Establecer atributos de la ventana:
Tamaño,
Posición,
Color de fondo,
Marco: Sí/No,
....
- Métodos de la ventana:
Abrir ventana,
Cerrar ventana,
Mover ventana,
Redimiensionar ventana,
Minimizar ventana,
....

## Creación de una clase y uso de la misma.
Para crear una clase basta con lo siguiente:

~~~
class calculadora:
    # aquí iría el código de la clase.
    # trancuilo que ya lo explico.
    pass
~~~

Para usar esa clase:

~~~
    mi_caculadora = calculadora()
    print mi_caculadora
~~~

### Registros y Métodos en una clase.
- Un __registro__ es un valor que almacena ese objeto.
- Un __método__ es una función que realiza es método. Para definir un método:
~~~
class Mi_clase:
    def Mi_metodo(self, valor1='foo', valor2='bar'):
        pass
~~~

#### Método: __init__(self, valores)
El método __init__ es el primer método que se ejecuta al iniciar un objeto con una clase.
Si hago: 'mi_calculadora = calculadora()', lo que haya en ese método es lo primero que se ejecutará.
Ni que decir tiene que al crear un objeto le podemos pasar parámetros:

~~~
mi_calculadora = calculadora(23,15)
~~~

Inicio 'mi_calculadora' pasándole los valores: 23 y 15.

Esos dos números los podemos poner como valores iniciales para la calculadora de la siguiente forma:

~~~
class calculadora:
    def __init__(self, valor1, valor2):
        self.operando_1 = valor1
        self.operando_2 = valor2
~~~

##### ¿Qué es 'self'?
Buena pregunta.
__self__ hace referencia al propio objeto, NO a la clase. Esto es que si quiero modificar un valor de 'mi_calculadora' lo hago así:

~~~
class calculadora:
    def __init__(self, valor1, valor2):
        self.operando_1 = valor1
        self.operando_2 = valor2

# Función principal
mi_calculadora = calculadora(12,12)
# Modifico el segundo operando.
mi_calculadora.operando_2 = 23
~~~

Imaginemos que creamos 3 objetos calculadora:

~~~
class calculadora:
    def __init__(self, valor1, valor2):
        self.operando_1 = valor1
        self.operando_2 = valor2

# Función principal
bar = '1234'
mi_calculadora_1 = calculadora(10,2)
mi_calculadora_2 = calculadora(0,0)
mi_calculadora_3 = calculadora('foo',bar)
~~~

Si ahora quiero ver los valores que tiene almacenados una de ellas se hace así:

~~~
print mi_calculadora_3.operando_1
print mi_calculadora_3.operando_2
~~~

Eso devuelve: 'foo' y a continuación '1234'.

De la misma forma __self__ se usa también en los métodos.

#### Método: __str__(self)
El método __str__ es la cadena que se devuelve al llamar al objeto.

~~~
class calculadora:
    def __init__(self, valor1, valor2):
        self.operando_1 = valor1
        self.operando_2 = valor2

    def __str__(self):
        resultado = self.operando_1 + self.operando_2
        linea = "Operación: %d + %d = %d" %(self.operando_1, self.operando_2, resultado)
        return linea
# Aquí la función principal.
mi_calculadora = calculadora(2,3)
print mi_calculadora
~~~

Eso devuelve:

~~~
Operación: 2 + 3 = 5
~~~

